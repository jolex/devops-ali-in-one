#!/bin/bash

set -eu

#
# Prepare the client's stuff.
#
mkdir client
cd client

# Generate a private RSA key.
openssl genrsa -out key.pem 2048

# Generate a certificate from our private key.
openssl req -new -key key.pem -out req.pem -outform PEM -subj /CN=$(hostname)/O=client/ -nodes

# Sign the certificate with our CA.
cd ../testca-pilot
openssl ca -config openssl.cnf -in ../client/req.pem -out ../client/cert.pem -notext -batch -extensions client_ca_extensions

# Create a key store that will contain our certificate.
cd ../client
openssl pkcs12 -export -out key-store.p12 -in cert.pem -inkey key.pem -passout pass:MySecretPassword

# Create a trust store that will contain the certificate of our CA.
openssl pkcs12 -export -out trust-store.p12 -in ../testca-pilot/cacert.pem -inkey ../testca-pilot/private/cakey.pem -passout pass:rabbitstore

#Create a Java key store containing the server certificate protected with a password
#keytool -importcert -alias server001 -file ../server/cert.pem -keystore ../keystore/rabbit.jks -keypass rabbitstore

# Make files visible to broker
chmod 755 key-store.p12 trust-store.p12
